import React from "react";
import styled from 'styled-components';
import { Button } from "reactstrap";

const Tecla = styled.div`
    width: 50px;
    height: 50px;
    margin: 4px;
    background-color: gray;
    display: flex;
    justify-content: center;
    align-items: center;
    border-radius: 8px;
    color: white;
    font-weight: 700;
`

const valores = [1, 2, 3, 4, 5, 6, 7, 8, 9, 0];

export default (props) => {

    const teclasNum = valores.map(el => {
        return (
            <Tecla className="button" onClick={() => props.valor(el)}>{el}</Tecla>
        )
    })

    return (
        <>
            <div className="numericos">
                {teclasNum}
            </div>
            <div className="operadores">
                <Tecla onClick={() => props.signo("d")}>/</Tecla>
                <Tecla onClick={() => props.signo("m")}>*</Tecla>
                <Tecla onClick={() => props.signo("r")}>-</Tecla>
                <Tecla onClick={() => props.signo("s")}>+</Tecla>
            </div>
            <div className="operadores">
                <Tecla onClick={props.borrar}> C </Tecla>
                <div className="btn_lg" onClick={props.calcular}> = </div>
            </div>
        </>
    )
}