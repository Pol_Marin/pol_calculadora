import React, { useEffect, useState } from "react";

import 'bootstrap/dist/css/bootstrap.min.css';
import "./style.css";

import Pantalla from "./Pantalla";
import Teclado from "./Teclado";

function App() {

  /* STATES */

  const [valor1, setValor1] = useState("0");
  const [valor2, setValor2] = useState("0");

  const [signo, setSigno] = useState(" ");

  const [resultado, setResultado] = useState(0);

  const [mensaje, setMensaje] = useState(0);


  /* AÑADIR VALORES */

  const addValue = (x) => {

    const num = x.toString();

    if (signo === " ") {
      if (valor1 === "0") {
        setValor1(num);
      } else {
        setValor1(valor1 + num);
      }
    } else {
      if (valor2 === "0") {
        setValor2(num);
      } else {
        setValor2(valor2 + num);
      }
    }
  }
  
  /* AÑADIR SIGNO */

  const addSigno = (x) => setSigno(x);

  /* REALIZAR CALCULO */

  const calcular = () => {

    let v1 = parseInt(valor1);
    let v2 = parseInt(valor2);

    if (signo === "s") {
      setResultado(v1 + v2);
    }
    else if (signo === "r") {
      setResultado(v1 - v2);
    }
    else if (signo === "m") {
      setResultado(v1 * v2);
    }
    else if (signo === "d") {
      setResultado(v1 / v2);
    }
    setMensaje(resultado);
  }

/* BORRAR PANTALLA */

const deletePantalla = () => {
  setResultado(0);
  setValor1("0");
  setValor2("0");
  setSigno(" ")
  setMensaje(0);
}

/* MENSAJE */
useEffect(() => {

  if (signo === " ") {
    setMensaje(valor1);
  } else if (resultado !== 0) {
    setMensaje(resultado);
  } else {
    setMensaje(valor2);
  }
}, [valor1, valor2, resultado])

return (
  <>
    <div className="calculadora">
      <Pantalla mens={mensaje} />
      <Teclado valor={addValue} signo={addSigno} calcular={calcular} borrar={deletePantalla} />
    </div>
  </>
);
}

export default App;
